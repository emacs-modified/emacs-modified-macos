# Emacs Modified for macOS

**Emacs Modified for macOS** is a distribution of [GNU Emacs](https://www.gnu.org/software/emacs/) bundled with a few select packages for R developers and LaTeX users, most notably [ESS](https://ess.r-project.org) and [AUCTeX](https://www.gnu.org/software/auctex/). The distribution is based on GNU Emacs [compiled by David Caldwell](https://emacsformacosx.com).

Other than the few selected additions and some minor configuration, this is a stock distribution of Emacs. Users of Emacs on other platforms will appreciate the similar look and feel of the application.

The [official project page](https://emacs-modified.gitlab.io) provides detailed information on the distribution and links to binary releases.

## Repository content

The repository contains a few distribution-specific files and a `Makefile` to fetch the other components and combine everything into a disk image. The complete source code of Emacs and the packages is not hosted here.

## Prerequisites

Building the distribution on macOS requires `make`, `hdiutil`, `curl`, `git` and a number of standard Unix command line tools. Many of these utilities do not come bundled with the operating system. One first needs to install Xcode from the App Store and, second, to install the Command Line Developer Tools by launching from the Terminal the following command:

```bash
$ xcode-select --install
```

## Building the distribution

Edit the `Makeconf` file to set the version numbers of GNU Emacs, the distribution and the various packages. Then `make` will launch the following three main steps:

1. `get-packages` will fetch the binary release of GNU Emacs and the packages.

2. `emacs` will, in summary, decompress the [Emacs for Mac OS X](https://emacsformacosx.com) disk image in a temporary directory, add all the packages into the application tree and build a new signed application bundle.

3. `release` will notarize the disk image, upload it to the GitLab package registry, and create a release with the a link to the disk image in the release notes.

Each of the above three steps is split into smaller recipes, around 20 in total. See the `Makefile` for details.

## Publishing on GitLab

Uploading files and publishing a release on GitLab from the command line involves using the [GitLab API](https://docs.gitlab.com/ee/api/README.html). The interested reader may have a look at the `upload` and `create-release` recipes in the `Makefile` to see how we achieved complete automation of the process, including the extraction of the release notes from the `NEWS` file.

<!-- Local Variables: -->
<!-- eval: (auto-fill-mode -1) -->
<!-- eval: (visual-line-mode) -->
<!-- End: -->
